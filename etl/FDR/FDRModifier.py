from etl.Modifier import MODIFIER

class FDR_MODIFIER(MODIFIER):
    columns = ["Close"]

    @staticmethod
    def get_df_arg(df) -> str:
        df_arg = list(set(df["ArgName"]))[0]
        return df_arg

    def __call__(self, df):
        arg = self.get_df_arg(df)
        md_df = super().slice_df_columns(df=df, columns=self.columns).rename(columns={self.columns[0]: arg}).squeeze()
        return md_df


def get_fdr_modifier():
    fdr_modifier = FDR_MODIFIER()
    return fdr_modifier
