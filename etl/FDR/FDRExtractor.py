import pandas as pd
from etl.FDR.FDRBroker import get_fdr_broker


class FDR_EXTRACTOR:
    def __init__(self, fdr_broker) -> None:
        self.broker = fdr_broker

    def get_stock_ohlcv_df(self, stockcode, start_date, end_date) -> pd.DataFrame:
        """
        Stock OHLCV
            Open High Low Close Volume
        """
        df = self.broker.DataReader(symbol=stockcode, start=start_date, end=end_date)
        return df

    def get_dji_df(self, start_date, end_date) -> pd.DataFrame:
        """
        DJI
            Down Johns Index
            다우존스지수
        """
        df = self.broker.DataReader(symbol="DJI", start=start_date, end=end_date)
        df["ArgName"] = "dji"
        return df

    def get_nci_df(self, start_date, end_date) -> pd.DataFrame:
        """
        NCI
            Nasdaq Composite Index
            나스닥종합지수
        """
        df = self.broker.DataReader(symbol="IXIC", start=start_date, end=end_date)
        df["ArgName"] = "nci"
        return df

    def get_snp_df(self, start_date, end_date) -> pd.DataFrame:
        """
        SNP
            S&P 500
        """
        df = self.broker.DataReader(symbol="S&P500", start=start_date, end=end_date)
        df["ArgName"] = "snp"
        return df

    def get_hsi_df(self, start_date, end_date) -> pd.DataFrame:
        """
        HSI
            Hang Seng Index
            항셍지수
        """
        df = self.broker.DataReader(symbol="HSI", start=start_date, end=end_date)
        df["ArgName"] = "hsi"
        return df

    def get_nki_df(self, start_date, end_date) -> pd.DataFrame:
        """
        NKI
            NikKei Index
            닛케이지수
        """
        df = self.broker.DataReader(symbol="N225", start=start_date, end=end_date)
        df["ArgName"] = "nki"
        return df

    def get_crudoil_df(self, start_date, end_date) -> pd.DataFrame:
        """
        Crud Oil
            원유
        """
        df = self.broker.DataReader(symbol="CL=F", start=start_date, end=end_date)
        df["ArgName"] = "crudoil"
        return df

    def get_gld_df(self, start_date, end_date) -> pd.DataFrame:
        """
        Gold
            금
        """
        df = self.broker.DataReader(symbol="GC=F", start=start_date, end=end_date)
        df["ArgName"] = "gld"
        return df

    def get_usd_krw_df(self, start_date, end_date) -> pd.DataFrame:
        """
        usd_krw
            원/달러 환율
        """
        df = self.broker.DataReader(symbol="USD/KRW", start=start_date, end=end_date)
        df["ArgName"] = "usd_krw"
        return df

    def get_jpy_krw_df(self, start_date, end_date) -> pd.DataFrame:
        """
        jpy_krw_df
            원/엔 환율
        """
        df = self.broker.DataReader(symbol="JPY/KRW", start=start_date, end=end_date)
        df["ArgName"] = "jpy_krw"
        return df

    def get_info_df(self, start_date, end_date) -> pd.DataFrame:
        """
        주간실업청구건수 : ICSA
            -> URR : UnRate Request
        소비자심리지수 : UMCSENT
            -> CESI : Customer Economic Sentiment Index
        주택판매지수 : HSN1F
            -> HSI : House Sell Index
        실업률 : UNRATE
            -> UR : UnRate
        M2 통화량 : M2SL
            -> CFI : Cash Flow Index
        """
        rename_dict = {
            "ICSA": "URR",
            "UMCSENT": "CESI",
            "HSN1F": "HSI",
            "UNRATE": "UR",
            "M2SL": "CFI",
        }
        info_df = self.broker.DataReader(symbol="FRED:ICSA,UMCSENT,HSN1F,UNRATE,M2SL", start=start_date, end=end_date)
        info_df.rename(columns=rename_dict, inplace=True)
        return info_df


def get_fdr_extractor():
    fdr_extractor = FDR_EXTRACTOR(get_fdr_broker())
    return fdr_extractor
