from etl.Modifier import MODIFIER

class DART_FSS_MODIFIER(MODIFIER):
    columns = ["corp_code", "stock_code", "sector", "product"]
    index_column = "stock_code"

    def __call__(self, df):
        md_df = super().slice_df_columns(df, self.columns)
        md_df = super().set_df_index(md_df, self.index_column)
        return md_df


class DART_API_MODIFIER(MODIFIER):
    columns = ["stock_code", "fs_nm", "account_nm", "thstrm_amount"]
    index_column = "stock_code"

    @staticmethod
    def value2numeric(value):
        if value == "-":
            return None
        else:
            value = int(value.replace(",", ""))
            return value

    def __call__(self, df):
        md_df = super().slice_df_columns(df, self.columns)
        md_df = super().set_df_index(md_df, self.index_column)
        md_df["thstrm_amount"] = md_df["thstrm_amount"].apply(lambda x: self.value2numeric(x))
        md_df.dropna(inplace=True)
        return md_df
    
def get_dart_fss_modifier():
    dart_fss_modifier = DART_FSS_MODIFIER()
    return dart_fss_modifier

def get_dart_api_modifier():
    dart_api_modifier = DART_API_MODIFIER()
    return dart_api_modifier
