from etl.private import DART_PRIVATE
import dart_fss as DART_FSS_BROKER


class DART_API_BROKER:
    def __init__(self, api_key) -> None:
        self.api_key = api_key

    def get_corp_fundamental_url(self, corp_code, year, reprt_code):
        """
        1분기보고서 : 11013
        반기보고서 : 11012
        3분기보고서 : 11014
        사업보고서 : 11011
        """
        url = (
            f"https://opendart.fss.or.kr/api/fnlttSinglAcnt.json?"
            f"crtfc_key={self.api_key}&"
            f"corp_code={corp_code}&"
            f"bsns_year={year}&"
            f"reprt_code={reprt_code}"
        )
        return url

    def get_corps_fundamental_urls(self, corp_codes, year, reprt_code):
        """
        1분기보고서 : 11013
        반기보고서 : 11012
        3분기보고서 : 11014
        사업보고서 : 11011
        """
        urls = list()

        # api corp_codes maximum 100
        corp_codes_zips = self.get_chunked_list(corp_codes, 99)

        for corp_codes_zip in corp_codes_zips:
            url = (
                f"https://opendart.fss.or.kr/api/fnlttMultiAcnt.json?"
                f"crtfc_key={self.api_key}&"
                f"corp_code={','.join(corp_codes_zip)}&"
                f"bsns_year={year}&"
                f"reprt_code={reprt_code}"
            )
            urls.append(url)
        return urls

    @staticmethod
    def get_chunked_list(_list, n):
        chunked_list = [_list[i : i + n] for i in range(0, len(_list), n)]
        return chunked_list


def get_dart_fss_broker():
    DART_FSS_BROKER.set_api_key(DART_PRIVATE.api_key)
    return DART_FSS_BROKER


def get_dart_api_broker():
    dart_api_broker = DART_API_BROKER(DART_PRIVATE.api_key)
    return dart_api_broker
