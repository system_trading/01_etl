import requests
import pandas as pd
from etl.Dart.DartBroker import get_dart_api_broker, get_dart_fss_broker


class DART_FSS_EXTRACTOR:
    def __init__(self, dart_fss_broker) -> None:
        self.broker = dart_fss_broker

    def get_corp_info_df(self) -> pd.DataFrame:
        corp_list = self.broker.get_corp_list()
        raw_corp_info_df = pd.DataFrame([corp.to_dict() for corp in corp_list._corps])
        corp_info_df = raw_corp_info_df[
            ~(raw_corp_info_df["stock_code"].isna()) & ~(raw_corp_info_df["sector"].isna())
        ].copy()
        return corp_info_df


class DART_API_EXTRACTOR:
    def __init__(self, dart_api_broker) -> None:
        self.broker = dart_api_broker

    def get_corp_fundamental_df(self, corp_code, year, reprt_code) -> pd.DataFrame:
        url = self.broker.get_corp_fundamental_url(corp_code, year, reprt_code)
        resp = requests.get(url=url)
        corp_fundamental_df = pd.DataFrame(resp.json()["list"])
        return corp_fundamental_df

    def get_corps_fundamental_df(self, corp_codes, year, reprt_code) -> pd.DataFrame:
        urls = self.broker.get_corps_fundamental_urls(corp_codes, year, reprt_code)
        _corps_fundamental_df_list = list()

        for url in urls:
            try:
                resp = requests.get(url=url)
                _corps_fundamental_df = pd.DataFrame(resp.json()["list"])
                _corps_fundamental_df_list.append(_corps_fundamental_df)
            except:
                pass
        corps_fundamental_df = pd.concat(_corps_fundamental_df_list, axis=0)
        return corps_fundamental_df


def get_dart_fss_extractor():
    dart_fss_extractor = DART_FSS_EXTRACTOR(get_dart_fss_broker())
    return dart_fss_extractor


def get_dart_api_extractor():
    dart_api_extractor = DART_API_EXTRACTOR(get_dart_api_broker())
    return dart_api_extractor
