class MODIFIER:
    @staticmethod
    def rename_df_column(df, rename_dict):
        md_df = df.rename(columns=rename_dict)
        return md_df

    @staticmethod
    def slice_df_columns(df, columns):
        md_df = df.loc[:, columns]
        return md_df

    @staticmethod
    def set_df_index(df, column):
        md_df = df.set_index(column)
        return md_df