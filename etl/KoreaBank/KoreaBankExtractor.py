import datetime as dt
from dateutil.relativedelta import relativedelta
import pandas as pd

from etl.KoreaBank.KoreaBankBroker import get_koreabank_broker


def monthly_extractor(func):
    """
    Ex)
        2023-12-20 -> 2023-11
    """

    def wrapper(self, start_date, end_date, *arg, **kwargs):
        def _monthly_formatter(date):
            _date = dt.datetime.strptime(date, "%Y-%m-%d") - relativedelta(months=1)
            formmated_date = _date.strftime("%Y%m")
            return formmated_date

        formmated_start_date = _monthly_formatter(start_date)
        formmated_end_date = _monthly_formatter(end_date)
        return func(self, formmated_start_date, formmated_end_date, *arg, **kwargs)

    return wrapper


def daily_extractor(func):
    """
    Ex)
        2023-12-20 -> 2023-12-19
    """

    def wrapper(self, start_date, end_date, *arg, **kwargs):
        def _daily_formatter(date):
            _date = dt.datetime.strptime(date, "%Y-%m-%d") - relativedelta(days=1)
            formmated_date = _date.strftime("%Y%m%d")
            return formmated_date

        formmated_start_date = _daily_formatter(start_date)
        formmated_end_date = _daily_formatter(end_date)
        return func(self, formmated_start_date, formmated_end_date, *arg, **kwargs)

    return wrapper


class KOREABANK_EXTRACTOR:
    def __init__(self, koreabank_broker) -> None:
        self.broker = koreabank_broker

    @monthly_extractor
    def get_cpi_df(self, start_date, end_date, all=False) -> pd.DataFrame:
        """
        @monthly_extractor
        CPI :
            Customer Price Index
            소비자 물가지수
        """
        if all:
            cpi_df = self.broker.get_statistic_search(통계표코드="901Y009", 주기="M", 검색시작일자=start_date, 검색종료일자=end_date)
        else:
            cpi_df = self.broker.get_statistic_search(
                통계표코드="901Y009", 통계항목코드1="0", 주기="M", 검색시작일자=start_date, 검색종료일자=end_date
            )
        cpi_df["ArgName"] = "cpi"
        return cpi_df

    @monthly_extractor
    def get_ppi_df(self, start_date, end_date, all=False) -> pd.DataFrame:
        """
        @monthly_extractor
        PPI :
            Product Price Index
            생산자물가지수
        """
        if all:
            ppi_df = self.broker.get_statistic_search(통계표코드="404Y016", 주기="M", 검색시작일자=start_date, 검색종료일자=end_date)
        else:
            ppi_df = self.broker.get_statistic_search(
                통계표코드="404Y016", 통계항목코드1="*AA", 주기="M", 검색시작일자=start_date, 검색종료일자=end_date
            )
        ppi_df["ArgName"] = "ppi"
        return ppi_df

    @monthly_extractor
    def get_esi_df(self, start_date, end_date) -> pd.DataFrame:
        """
        @monthly_extractor
        ESI :
            Economic sentiment indicator (ESI)
            경제심리지수
        100 < esi : good
        esi < 100 : bad
        """
        esi_df = self.broker.get_statistic_search(
            통계표코드="513Y001", 통계항목코드1="E1000", 주기="M", 검색시작일자=start_date, 검색종료일자=end_date
        )
        esi_df["ArgName"] = "esi"
        return esi_df

    @monthly_extractor
    def get_front_cei_df(self, start_date, end_date) -> pd.DataFrame:
        """
        @monthly_extractor
        [선행종합지수]
        CEI :
            Composite Economic Index
            경기종합지수
        100 < cei : good
        cei < 100 : bad
        """
        front_cei_df = self.broker.get_statistic_search(
            통계표코드="901Y067", 통계항목코드1="I16A", 주기="M", 검색시작일자=start_date, 검색종료일자=end_date
        )
        front_cei_df["ArgName"] = "front_cei"
        return front_cei_df

    @monthly_extractor
    def get_mid_cei_df(self, start_date, end_date) -> pd.DataFrame:
        """
        @monthly_extractor
        [동행종합지수]
        CEI :
            Composite Economic Index
            경기종합지수
        100 < cei : good
        cei < 100 : bad
        """
        mid_cei_df = self.broker.get_statistic_search(
            통계표코드="901Y067", 통계항목코드1="I16B", 주기="M", 검색시작일자=start_date, 검색종료일자=end_date
        )
        mid_cei_df["ArgName"] = "mid_cei"
        return mid_cei_df

    @monthly_extractor
    def get_back_cei_df(self, start_date, end_date) -> pd.DataFrame:
        """
        @monthly_extractor
        [후행종합지수]
        CEI :
            Composite Economic Index
            경기종합지수
        100 < cei : good
        cei < 100 : bad
        """
        back_cei_df = self.broker.get_statistic_search(
            통계표코드="901Y067", 통계항목코드1="I16C", 주기="M", 검색시작일자=start_date, 검색종료일자=end_date
        )
        back_cei_df["ArgName"] = "back_cei"
        return back_cei_df

    @monthly_extractor
    def get_deposits_df(self, start_date, end_date) -> pd.DataFrame:
        """
        @monthly_extractor
        Deposits :
            예탁금
        - 예탁금 : 증권사에 넣어둔 돈
        """
        deposits_df = self.broker.get_statistic_search(
            통계표코드="901Y056", 통계항목코드1="S23A", 주기="M", 검색시작일자=start_date, 검색종료일자=end_date
        )
        deposits_df["ArgName"] = "deposits"
        return deposits_df

    @monthly_extractor
    def get_receivables_df(self, start_date, end_date) -> pd.DataFrame:
        """
        @monthly_extractor
        Receivables :
            미수금
        - 미수금 : 증권사에 빌린 돈
        """
        receivables_df = self.broker.get_statistic_search(
            통계표코드="901Y056", 통계항목코드1="S23D", 주기="M", 검색시작일자=start_date, 검색종료일자=end_date
        )
        receivables_df["ArgName"] = "receivables"
        return receivables_df

    @daily_extractor
    def get_nsi_df(self, start_date, end_date) -> pd.DataFrame:
        """
        @daily_extractor
        NSI :
            News Sentiment Index
            뉴스심리지수
        100 < nsi : good
        nsi < 100 : bad
        """
        nsi_df = self.broker.get_statistic_search(
            통계표코드="521Y001", 통계항목코드1="A001", 주기="D", 검색시작일자=start_date, 검색종료일자=end_date
        )
        nsi_df["ArgName"] = "nsi"
        return nsi_df

    @daily_extractor
    def get_kospi_df(self, start_date, end_date) -> pd.DataFrame:
        """
        @daily_extractor
        KOSPI 지수
        """
        kospi_df = self.broker.get_statistic_search(
            통계표코드="802Y001", 통계항목코드1="0001000", 주기="D", 검색시작일자=start_date, 검색종료일자=end_date
        )
        kospi_df["ArgName"] = "kospi"
        return kospi_df

    @daily_extractor
    def get_kosdaq_df(self, start_date, end_date) -> pd.DataFrame:
        """
        @daily_extractor
        KOSDAQ 지수
        """
        kosdaq_df = self.broker.get_statistic_search(
            통계표코드="802Y001", 통계항목코드1="0089000", 주기="D", 검색시작일자=start_date, 검색종료일자=end_date
        )
        kosdaq_df["ArgName"] = "kosdaq"
        return kosdaq_df

    @daily_extractor
    def get_ntb_10_df(self, start_date, end_date) -> pd.DataFrame:
        """
        @daily_extractor
        국고채 10년물
        """
        ntb_10_df = self.broker.get_statistic_search(
            통계표코드="817Y002", 통계항목코드1="010210000", 주기="D", 검색시작일자=start_date, 검색종료일자=end_date
        )
        ntb_10_df["ArgName"] = "ntb_10"
        return ntb_10_df


def get_koreabank_extractor():
    koreabank_extractor = KOREABANK_EXTRACTOR(get_koreabank_broker())
    return koreabank_extractor
