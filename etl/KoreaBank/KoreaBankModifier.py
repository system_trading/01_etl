import pandas as pd
from etl.Modifier import MODIFIER

class KoreaBank_MODIFIER(MODIFIER):
    rename_dict = {
        "시점": "Date",
        "값": "Value",
    }
    index_column = "Date"

    def format_datetime(self, df):
        try:
            df.index = pd.to_datetime(df.index, format="%Y%m%d")
        except:
            df.index = pd.to_datetime(df.index, format="%Y%m")
        return df

    @staticmethod
    def get_df_arg(df):
        df_arg = list(set(df["ArgName"]))[0]
        return df_arg

    def __call__(self, df):
        self.rename_dict["값"] = self.get_df_arg(df)
        md_df = super().rename_df_column(df=df, rename_dict=self.rename_dict)
        md_df = super().slice_df_columns(df=md_df, columns=list(self.rename_dict.values()))
        md_df = super().set_df_index(df=md_df, column=self.index_column)
        md_df = self.format_datetime(md_df)
        return md_df


def get_koreabank_modifier():
    koreabank_modifier = KoreaBank_MODIFIER()
    return koreabank_modifier
