import pandas as pd
from tqdm import tqdm


class ETL:
    def __init__(self, extractor, modifier, CFG) -> None:
        self.extractor = extractor
        self.modifier = modifier
        self.CFG = CFG


class KOREABANK_ETL(ETL):
    def get_etl_monthly_args(self):
        cpi_df = self.modifier(
            self.extractor.get_cpi_df(start_date=self.CFG["start_date"], end_date=self.CFG["end_date"]),
        )

        ppi_df = self.modifier(
            self.extractor.get_ppi_df(start_date=self.CFG["start_date"], end_date=self.CFG["end_date"]),
        )

        esi_df = self.modifier(
            self.extractor.get_esi_df(start_date=self.CFG["start_date"], end_date=self.CFG["end_date"]),
        )
        back_cei_df = self.modifier(
            self.extractor.get_back_cei_df(start_date=self.CFG["start_date"], end_date=self.CFG["end_date"])
        )
        mid_cei_df = self.modifier(
            self.extractor.get_mid_cei_df(start_date=self.CFG["start_date"], end_date=self.CFG["end_date"])
        )
        front_cei_df = self.modifier(
            self.extractor.get_front_cei_df(start_date=self.CFG["start_date"], end_date=self.CFG["end_date"])
        )

        deposits_df = self.modifier(
            self.extractor.get_deposits_df(start_date=self.CFG["start_date"], end_date=self.CFG["end_date"])
        )
        receivables_df = self.modifier(
            self.extractor.get_receivables_df(start_date=self.CFG["start_date"], end_date=self.CFG["end_date"])
        )
        monthly_df = pd.concat(
            [cpi_df, ppi_df, esi_df, back_cei_df, mid_cei_df, front_cei_df, deposits_df, receivables_df], axis=1
        )
        return monthly_df

    def get_etl_daily_args(self):
        nsi_df = self.modifier(
            self.extractor.get_nsi_df(start_date=self.CFG["start_date"], end_date=self.CFG["end_date"])
        )
        kospi_df = self.modifier(
            self.extractor.get_kospi_df(start_date=self.CFG["start_date"], end_date=self.CFG["end_date"])
        )
        kosdaq_df = self.modifier(
            self.extractor.get_kosdaq_df(start_date=self.CFG["start_date"], end_date=self.CFG["end_date"])
        )
        ntb_10_df = self.modifier(
            self.extractor.get_ntb_10_df(start_date=self.CFG["start_date"], end_date=self.CFG["end_date"])
        )
        daily_df = pd.concat([nsi_df, kospi_df, kosdaq_df, ntb_10_df], axis=1)
        daily_df = daily_df.ffill().bfill()
        return daily_df


class FDR_ETL(ETL):
    def get_exchange_ratio_df(self):
        """
        환율 관련 정보
            : usd / krw
            : jpy / krw
        """
        usd_krw_df = self.modifier(
            self.extractor.get_usd_krw_df(start_date=self.CFG["start_date"], end_date=self.CFG["end_date"])
        )
        jpy_krw_df = self.modifier(
            self.extractor.get_jpy_krw_df(start_date=self.CFG["start_date"], end_date=self.CFG["end_date"])
        )

        exchange_ratio_df = pd.concat([usd_krw_df, jpy_krw_df], axis=1)
        exchange_ratio_df = exchange_ratio_df.ffill().bfill()
        return exchange_ratio_df

    def get_global_index_df(self):
        """
        종합 지수
            : dji / 다우존스
            : nci / 나스닥
            : snp
            : hsi / 항셍지수
            : nki / 닛케이
        """
        # 미국
        dji_df = self.modifier(
            self.extractor.get_dji_df(start_date=self.CFG["start_date"], end_date=self.CFG["end_date"])
        )
        nci_df = self.modifier(
            self.extractor.get_nci_df(start_date=self.CFG["start_date"], end_date=self.CFG["end_date"])
        )
        snp_df = self.modifier(
            self.extractor.get_snp_df(start_date=self.CFG["start_date"], end_date=self.CFG["end_date"])
        )

        # 홍콩
        hsi_df = self.modifier(
            self.extractor.get_hsi_df(start_date=self.CFG["start_date"], end_date=self.CFG["end_date"])
        )

        # 일본
        nki_df = self.modifier(
            self.extractor.get_nki_df(start_date=self.CFG["start_date"], end_date=self.CFG["end_date"])
        )
        global_index_df = pd.concat([dji_df, nci_df, snp_df, hsi_df, nki_df], axis=1)
        global_index_df = global_index_df.ffill().bfill()
        return global_index_df

    def get_material_df(self):
        """
        원자재
            : crudoil / 원유
            : gld / 금
        """
        crudoil_df = self.modifier(
            self.extractor.get_crudoil_df(start_date=self.CFG["start_date"], end_date=self.CFG["end_date"])
        )
        gld_df = self.modifier(
            self.extractor.get_gld_df(start_date=self.CFG["start_date"], end_date=self.CFG["end_date"])
        )
        material_df = pd.concat([crudoil_df, gld_df], axis=1)
        material_df = material_df.ffill().bfill()
        return material_df

    def get_stock_ohlcv_df(self, stock_codes):
        stock_ohlcv_df_list = list()
        for stock_code in tqdm(stock_codes):
            try:
                _stock_ohlcv_df = self.extractor.get_stock_ohlcv_df(
                    stock_code, start_date=self.CFG["start_date"], end_date=self.CFG["end_date"]
                )
                _stock_ohlcv_df["stock_code"] = stock_code
                stock_ohlcv_df_list.append(_stock_ohlcv_df)
            except:
                pass
        stock_ohlcv_df = pd.concat(stock_ohlcv_df_list, axis=0)
        return stock_ohlcv_df


class DART_ETL:
    """
    CFG : {
        "year":"2023"
        "reprt_code" : "11014"
    }
    1분기보고서 / 11013
    반기보고서 / 11012
    3분기보고서 / 11014
    사업보고서 / 11011
    """

    def __init__(self, fss_extractor, fss_modifier, api_extractor, api_modifier, CFG) -> None:
        self.fss_extractor = fss_extractor
        self.fss_modifier = fss_modifier
        self.api_extractor = api_extractor
        self.api_modifier = api_modifier
        self.CFG = CFG

    def get_corp_info_df(self):
        corp_info_df = self.fss_modifier(self.fss_extractor.get_corp_info_df())
        # dart_api corp_codes dependency
        self.corp_codes = list(corp_info_df["corp_code"])
        return corp_info_df

    def get_fundamental_df(self):
        fundamental_df = self.api_modifier(
            self.api_extractor.get_corps_fundamental_df(
                corp_codes=self.corp_codes, year=self.CFG["year"], reprt_code=self.CFG["reprt_code"]
            )
        )
        fundamental_df["meta"] = str(self.CFG["year"]) + "_" + str(self.CFG["reprt_code"])
        return fundamental_df


class PYKRX_ETL(ETL):
    def get_kospi_trader_df(self):
        kospi_trader_df = self.modifier(
            self.extractor.get_kospi_trading_df(start_date=self.CFG["start_date"], end_date=self.CFG["end_date"])
        )
        return kospi_trader_df

    def get_kosdaq_trader_df(self):
        kosdaq_trader_df = self.modifier(
            self.extractor.get_kosdaq_trading_df(start_date=self.CFG["start_date"], end_date=self.CFG["end_date"])
        )
        return kosdaq_trader_df
