import pandas as pd
from .PyKRXBroker import get_pykrx_broker


class PYKRX_EXTRACTOR:
    def __init__(self, pykrx_broker) -> None:
        self.broker = pykrx_broker

    def get_stock_trading_df(self, stock_code, start_date, end_date) -> pd.DataFrame:
        stock_trading_df = self.broker.get_market_trading_value_by_date(
            ticker=stock_code, fromdate=start_date, todate=end_date
        )
        return stock_trading_df

    def get_kospi_trading_df(self, start_date, end_date) -> pd.DataFrame:
        kospi_trading_df = self.get_stock_trading_df(stock_code="KOSPI", start_date=start_date, end_date=end_date)
        return kospi_trading_df

    def get_kosdaq_trading_df(self, start_date, end_date) -> pd.DataFrame:
        kosdaq_trading_df = self.get_stock_trading_df(stock_code="KOSDAQ", start_date=start_date, end_date=end_date)
        return kosdaq_trading_df

    def get_stock_fundamental_df(self, stock_code, start_date, end_date) -> pd.DataFrame:
        stock_fundamental_df = self.broker.get_market_fundamental(
            ticker=stock_code,
            fromdate=start_date,
            todate=end_date,
        )
        return stock_fundamental_df

    def get_stock_short_df(self, stock_code, start_date, end_date) -> pd.DataFrame:
        stock_short_df = self.broker.get_shorting_value_by_date(
            ticker=stock_code,
            fromdate=start_date,
            todate=end_date,
        )
        return stock_short_df


def get_pykrx_extractor():
    pykrx_extractor = PYKRX_EXTRACTOR(get_pykrx_broker())
    return pykrx_extractor

