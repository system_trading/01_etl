from ..Modifier import MODIFIER


class PYKRX_MODIFIER(MODIFIER):
    rename_dict = {
        "기관합계": "corp",
        "외국인합계": "foriegn",
        "개인": "indivisual",
    }
    index_column = "Date"

    def __call__(self, df):
        md_df = super().rename_df_column(df=df, rename_dict=self.rename_dict)
        md_df = super().slice_df_columns(df=md_df, columns=list(self.rename_dict.values()))
        md_df.index.name = self.index_column
        return md_df


def get_pykrx_modifier():
    pykrx_modifier = PYKRX_MODIFIER()
    return pykrx_modifier
